using System;

namespace Baseballers
{
    public class BaseballGame
    {
        public int inning;
        public Team homeTeam;
        public Team awayTeam;        
        public int homeScore;
        public int awayScore;
        
        public Team teamAtBat;
        public Team teamDefending;  
        public double textDelayInSeconds = 0.01;

        public BaseballGame(Team home, Team away)
        {
            inning = 1;
            homeTeam = home;
            awayTeam = away;
            teamAtBat = away;
            teamDefending = home;
            teamAtBat.batterIndex = 0;
            teamDefending.batterIndex = 0;
            
            // Set the starting pitchers
            teamAtBat.activePitcher = teamAtBat.pitchers[PitcherPositions.Starting];
            teamDefending.activePitcher = teamDefending.pitchers[PitcherPositions.Starting];

            while(inning <= 9 || homeScore == awayScore)
            {
                if(inning % 3 == 0)
                {
                    // Progress to next pitcher every 3 innings
                    teamAtBat.activePitcher = teamAtBat.GetNextPitcherInLineup();
                    teamDefending.activePitcher = teamDefending.GetNextPitcherInLineup();
                }
                Console.WriteLine($"Top of inning {inning}. {teamAtBat.name} will bat, {teamDefending.name} on defense.");
                PlayHalfInning();
                teamAtBat = home;
                teamDefending = away;
                Console.WriteLine($"Bottom of inning {inning}. {teamAtBat.name} will bat, {teamDefending.name} on defense.");
                PlayHalfInning();
                teamAtBat = away;
                teamDefending = home;
                Console.WriteLine($@"That will end inning {inning}. 
                    {homeTeam.name} {homeScore}, {awayTeam.name} {awayScore}.");
                inning += 1;
            }
            
        }

        private void PlayHalfInning()
        {
            new HalfInning().Play(this, teamDefending.activePitcher, teamAtBat.batters[(BatterPositions)teamAtBat.batterIndex]);
        }
    }
}