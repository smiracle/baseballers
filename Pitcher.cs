using System;
using System.Collections.Generic;

namespace Baseballers
{
    public class Pitcher : Player
    {
        public int accuracy;
        public PitcherPositions position;
        public Pitcher() : base()
        {
            accuracy = Randomizer.GetRandom(1, 100);            
            position = PitcherPositions.Bench;
        }

        public string GetMinimalInfo()
        {
            return $"{firstName} {lastName} ({Names.pitcherShortPositions[position]})";
        }
        public void OutputAllStats()
        {
            Console.WriteLine($"{firstName} {lastName} ({position}) Acc {accuracy}");
        }
    }
}   