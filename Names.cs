using System;
using System.Collections.Generic;

namespace Baseballers
{
    public static class Names
    {
        public static Dictionary<PitcherPositions, string> pitcherShortPositions = new Dictionary<PitcherPositions, string>()
        {
            {PitcherPositions.Starting, "SP"},
            {PitcherPositions.Relief, "RP"},
            {PitcherPositions.Closer, "CP"},
            {PitcherPositions.Bench, "BP"}
        };

        public static Dictionary<BatterPositions, string> batterShortPositions = new Dictionary<BatterPositions, string>()
        {
            {BatterPositions.DesignatedHitter, "DH"},
            {BatterPositions.Catcher, "C"},
            {BatterPositions.FirstBase, "FB"},
            {BatterPositions.SecondBase, "SB"},
            {BatterPositions.ThirdBase, "TB"},
            {BatterPositions.ShortStop, "SS"},
            {BatterPositions.LeftField, "LF"},
            {BatterPositions.CenterField, "CF"},
            {BatterPositions.RightField, "RF"},
            {BatterPositions.Bench, "B"},
        };

        private static string[] TeamNames = {
            "The Demons",
            "The Angels",
            "The Bigleys",
            "The Tweedlers",
            "The Cacklebags",
            "The Savants",
            "The Astrophysicists",
            "The Bigfoots",
            "The Glaucoma",
            "The Weebs",
            "The Nerds",
            "The Lumberjacks",
            "The Oceanfront",
            "The Hurricanes",
            "The Wildebees",
            "The Regulators",
            "The Establishment",
            "The Jockies",
            "The Jabberwockies",
        };

        private static string[] FirstNames = {
            "Dave",
            "Bill",
            "Jimmy",
            "Hank",
            "Joey",
            "Frank",
            "Buster",
            "Timmy",
            "Cid",
            "Thor",
            "Mike",
            "Rob",
            "Earl",
            "Dean",
            "Ricardo",
            "Raul",
            "Bob",
            "Jack",
            "Tim",
            "Adam",
            "Chris",
            "Eric"
        };

        private static string[] LastNames = {
            "Johnson",
            "Miller",
            "Ciddly",
            "Whiteside",
            "Yardly",
            "Bonds",
            "Watts",
            "Cruise",
            "Buntly",
            "Springer",
            "Fee",
            "Ford",
            "Elder",
            "Younger",
            "Atouba",
            "Sanchez",
            "Rivera",
            "Diego",
            "Valasquez",
            "Buckholtz",
            "Stone",
        };

        public static string GetRandomTeamName()
        {
            return TeamNames[Randomizer.GetRandom(0, TeamNames.Length - 1)];
        }

        public static string GetRandomFirstName()
        {
            return FirstNames[Randomizer.GetRandom(0, FirstNames.Length - 1)];
        }

        public static string GetRandomLastName()
        {            
            return LastNames[Randomizer.GetRandom(0, LastNames.Length - 1)];
        }
    }
}