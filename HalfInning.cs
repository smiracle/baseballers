using System;
using System.Threading;

namespace Baseballers
{
    public class HalfInning
    {
        private int[] battersOnBases = new int[] { -1, -1, -1, -1 };
        public int outs;
        private int strikes;
        private int balls;
        private bool isHalfInningComplete;        
        private BaseballGame game;

        public void Play(BaseballGame game, Pitcher pitcher, Batter batter)
        {
            this.game = game;
            var isNextBatterNeeded = true;
            var nextBatter = batter;
            while(!isHalfInningComplete)
            {
                if(isNextBatterNeeded)
                {
                    balls = 0;
                    strikes = 0;
                    // Batter up
                    if(nextBatter == null)
                    {
                        nextBatter = game.teamAtBat.GetNextBatterInLineup();
                    }
                    Console.WriteLine();
                    Output($"{nextBatter.GetMinimalInfo()} steps up to the plate...");
                    SetBatterAtBase((int)Bases.Home, nextBatter);
                    nextBatter = null;
                    isNextBatterNeeded = false;
                }

                var randomPitch = Randomizer.GetRandom(1, 100);
                var pitchType = (PitchTypes)Randomizer.GetRandom(0, Enum.GetValues(typeof(PitchTypes)).Length);
                if(randomPitch + pitcher.accuracy > 100)
                {
                    // A fair pitch
                    isNextBatterNeeded = HandleFairPitch(pitcher, batter, pitchType);
                }
                else
                {
                    // Bad pitch
                    balls += 1;
                    Output($"{pitcher.GetMinimalInfo()} pitched a {pitchType}, but it went wide. {balls} ball(s), {strikes} strike(s).");
                    if(balls >= 4)
                    {
                        Output($"{batter.GetMinimalInfo()} will take a walk.");
                        SetBatterAtBase((int)Bases.First, GetBatterAtBase((int)Bases.Home));
                        SetBaseToEmpty((int)Bases.Home);
                        isNextBatterNeeded = true;              
                    }
                }
            }
        }

        private void Output(string output)
        {
            Console.WriteLine(output);
            // Slow things down for readability
            Thread.Sleep((int)(1000 * game.textDelayInSeconds));
        }

        private bool HandleFairPitch(Pitcher pitcher, Batter batter, PitchTypes pitchType)
        {
            var isNextBatterNeeded = false;
            var randomBat = Randomizer.GetRandom(1, 100);  
            Output($"{pitcher.GetMinimalInfo()} pitched a {pitchType}.");
            var randomHitDirection = (HitDirections)Randomizer.GetRandom(1, Enum.GetValues(typeof(HitDirections)).Length);
            var batScore = randomBat + batter.power;
            if(batScore > 190)
            {
                // Homerun
                Output($"{batter.GetMinimalInfo()} with a tremendous hit! It's outta here!");
                Homerun();
                return true;
            }
            else if(batScore > 180)
            {                
                // Potential triple
                isNextBatterNeeded = HandleHit("hit a big one!", 3, batter, randomHitDirection);                    
            }
            else if(batScore > 170)
            {
                // Potential double
                isNextBatterNeeded = HandleHit("with a solid hit!", 2, batter, randomHitDirection);
            }
            else if(batScore > 125)
            {
                // Potential single
                isNextBatterNeeded = HandleHit("with a decent hit.", 1, batter, randomHitDirection);
            }
            else
            {
                var randomSwing = Randomizer.GetRandom(1,3);
                strikes += 1;
                if(randomSwing == 1)
                {
                    Output($"{batter.GetMinimalInfo()} didn't swing in time. {balls} ball(s), {strikes} strike(s).");
                }
                else
                {
                    Output($"{batter.GetMinimalInfo()} with a swing and a miss. {balls} ball(s), {strikes} strike(s).");
                }

                if(strikes == 3)
                {
                    IncrementOuts();
                    Output($"{batter.GetMinimalInfo()} struck out. {outs} out(s).");
                    isNextBatterNeeded = true;
                }
            }
            return isNextBatterNeeded;
        }

        private bool HandleHit(string description, int hitTime, Batter batter, HitDirections direction)
        {
            var isNextBatterNeeded = false;
            Output($"{batter.GetMinimalInfo()} {description}");
            var isFoul = direction == HitDirections.FoulLeft || direction == HitDirections.FoulRight;
            if(isFoul)
            {
                if(strikes < 2)
                {
                    strikes += 1;
                }
                Output($"But it went foul {(direction == HitDirections.FoulLeft ? "to the left." : "to the right.")} {balls} ball(s), {strikes} strike(s).");
            }
            else
            {
                // It's not a homerun, so check if the ball was caught
                isNextBatterNeeded = true;
                var defender = GetFielderForDirection(direction);
                var catchScore = GetCatchScore(defender);
                var isCatch = HandleCatch(catchScore, defender);
                if(isCatch)
                {                    
                    IncrementOuts();
                    // Batter is instantly out
                    SetBaseToEmpty((int)Bases.Home);                                    
                    return true;
                }

                // Check how quickly the fielder retrieved the ball
                var retrievalTime = GetBallRetrievalTime(defender, hitTime, isCatch);
                var ticks = 0;
                var ballAtBaseIndex = -1;
                var previousBallAtBaseIndex = -1;
                while(GetOccupiedBaseCount() >= 1 && ticks <= retrievalTime + 1)
                {
                    // Advance time by one tick
                    ticks += 1;

                    if(retrievalTime <= ticks)
                    {
                        // The defending team is able to throw it to a base during this tick
                        // Update where the ball will be for the next advancement                        
                        var newBallAtBaseIndex = (int)GetOptimalBaseForThrow(direction, defender);
                        if(newBallAtBaseIndex != ballAtBaseIndex)
                        {
                            // Only throw if the ball isn't already there
                            ballAtBaseIndex = newBallAtBaseIndex;
                            Console.WriteLine($"{defender.GetMinimalInfo()} throws to {((Bases)ballAtBaseIndex).ToString().ToLower()}.");
                        }
                    }

                    // Advance the players once and check if basemen were able to tag a player in time
                    AdvanceTimeOnce(previousBallAtBaseIndex, ballAtBaseIndex);

                    // Allow players to remember where the ball was so they can stop where they are
                    previousBallAtBaseIndex = ballAtBaseIndex;
                }
            }

            return isNextBatterNeeded;
        }

        private void AdvanceTimeOnce(int previousBallLocationIndex, int ballBaseLocationIndex)
        {
            var isNextBatterNeeded = false;
            var ballLocation = (Bases)ballBaseLocationIndex;
            Batter tempBatter = null;
            if(IsBatterAtBase((int)Bases.Home))
            {
                tempBatter = GetBatterAtBase((int)Bases.Home);
                // The batter is definitely going to either move or be out, so we'll need a new one
                isNextBatterNeeded = true;
            }
            
            if(previousBallLocationIndex != (int)Bases.Home)
            {
                if(IsBatterAtBase(Bases.Third))
                {
                    // Move batter to Home from 3rd base if the ball wasn't there previously
                    var batterToMove = GetBatterAtBase((int)Bases.Third);
                    Output($"{batterToMove.GetMinimalInfo()} is running for home!");
                    SetBatterAtBase((int) Bases.Home, GetBatterAtBase((int)Bases.Third));
                    SetBaseToEmpty((int)Bases.Third);
                }
            }
            if(ballLocation != Bases.Third && previousBallLocationIndex != (int)Bases.Third
            && !IsBatterAtBase((int)Bases.Third))
            {
                if(IsBatterAtBase(Bases.Second))
                {
                    // Move batter to 3rd base from 2nd if unblocked by players and balls
                    var batterToMove = GetBatterAtBase((int)Bases.Second);
                    Output($"{batterToMove.GetMinimalInfo()} advances to third base.");
                    SetBatterAtBase((int) Bases.Third, batterToMove);
                    SetBaseToEmpty((int)Bases.Second);
                }
            }
            if(ballLocation != Bases.Second  && previousBallLocationIndex != (int)Bases.Second
             && !IsBatterAtBase((int)Bases.Second))
            {
                if(IsBatterAtBase(Bases.First))
                {
                    // Move batter to 2nd base from 1st if unblocked by players and balls
                    var batterToMove = GetBatterAtBase((int)Bases.First);
                    Output($"{batterToMove.GetMinimalInfo()} advances to second base.");
                    SetBatterAtBase((int) Bases.Second, batterToMove);
                    SetBaseToEmpty((int)Bases.First);
                }
            }
            if(tempBatter != null && ballLocation != Bases.First && previousBallLocationIndex != (int)Bases.First)
            {
                if(IsBatterAtBase((int) Bases.First))
                {
                    // First is still occupied, so the batter is out
                    IncrementOuts();
                    Output($"Nowhere for {tempBatter.GetMinimalInfo()} to run, he's out! {outs} out(s).");
                    if(IsBatterAtBase(Bases.Home) && GetBatterAtBase((int)Bases.Home) == tempBatter)
                    {
                        SetBaseToEmpty((int)Bases.Home);
                    }
                }
                else
                {
                     // Move the batter to 1st base from Home since they're unblocked
                    if(tempBatter != null)
                    {
                        Output($"{tempBatter.GetMinimalInfo()} advances to first base.");
                        SetBatterAtBase((int)Bases.First, tempBatter);
                        if(IsBatterAtBase(Bases.Home) && GetBatterAtBase((int)Bases.Home) == tempBatter)
                        {
                            SetBaseToEmpty((int)Bases.Home);
                        }
                    }
                }
            }

            isNextBatterNeeded = CalculateTagOut(previousBallLocationIndex, (int)ballLocation) ? true : isNextBatterNeeded;

            if(ballLocation != Bases.Home)
            {
                // The ball isn't at home, so increase the score if a runner made it home
                if(IsBatterAtBase((int)Bases.Home))
                {
                    if(game.teamAtBat == game.homeTeam)
                    {
                        game.homeScore += 1;
                    }
                    else
                    {
                        game.awayScore += 1;
                    }
                    Output($@"{GetBatterAtBase((int)Bases.Home).GetMinimalInfo()} successfully ran home!
                        {game.homeTeam.name} {game.homeScore}, {game.awayTeam.name} {game.awayScore}.");
                    SetBaseToEmpty((int)Bases.Home);
                }
            }
        }

        private bool CalculateTagOut(int previousBallLocationIndex, int ballLocation)
        {
            if(ballLocation == -1 || !IsBatterAtBase(ballLocation))
            {
                // Still in the outfield or no batters at that base
                return false;
            }
            var random = Randomizer.GetRandom(1, 100);
            var baseman = GetBasemanForPosition((int)ballLocation);

            // Factor the baseman's vision & speed and the running batter's speed into the chance to tag him out
            var score =  random + ((baseman.vision + baseman.speed) / 2) - GetBatterAtBase(ballLocation).speed / 2;
            if(previousBallLocationIndex == -1)
            {
                // Somewhat significant chance of getting an out since it was just thrown from outfield
                if(score > 100)
                {
                    Output($"{baseman.GetMinimalInfo()} scoops it up and tags {GetBatterAtBase(ballLocation).GetMinimalInfo()} out! {outs} out(s).");
                    SetBaseToEmpty(ballLocation);
                    IncrementOuts();
                    return true;
                }
            }
            else
            {
                // Much lower chance of getting another out as batters have had more time to run
                if(score > 150)
                {
                    Output($"{baseman.GetMinimalInfo()} with a great catch, he tags {GetBatterAtBase(ballLocation).GetMinimalInfo()} out!  {outs} out(s).");
                    SetBaseToEmpty(ballLocation);
                    IncrementOuts();
                    return true;
                }
            }
            return false;
        }

        private int GetBallRetrievalTime(Batter fielder, int hitTime, bool isCatch)
        {            
            var retrievalScore = Randomizer.GetRandom(1, 100) + fielder.speed;            
            if(isCatch || retrievalScore > 125)
            {
                // Retrieval was quick
                return hitTime - 1;
            }
            else if(retrievalScore > 75)
            {
                // Retrieval was average
                return hitTime;
            }
            else
            {
                // Retrieval was slow
                return hitTime + 1;
            }
        }

        private void Homerun()
        {
            var scoreIncrease = 0;
            balls = 0;
            strikes = 0;
            for(int i = 0; i < battersOnBases.Length - 1; i++)
            {
                if(battersOnBases[i] != -1)
                {
                    scoreIncrease += 1;
                }
                // Empty all the bases
                battersOnBases[i] = -1;
            }

            if(game.teamAtBat == game.homeTeam)
            {
                game.homeScore += scoreIncrease;
            }
            else
            {
                game.awayScore += scoreIncrease;
            }
            Output($@"{game.homeTeam.name} {game.homeScore}, {game.awayTeam.name} {game.awayScore}.");
        }

        private Bases GetBaseDefenderClosestTo(Batter defender)
        {
            // Does this make sense? Not really.
            switch(defender.position)
            {
                case BatterPositions.Catcher:
                    return Bases.Second;
                case BatterPositions.SecondBase:
                    return Bases.First;
                case BatterPositions.ThirdBase:
                    return Bases.Second;
                case BatterPositions.LeftField:
                    return Bases.Third;
                case BatterPositions.CenterField:
                    return Bases.Second;
                case BatterPositions.RightField:
                    return Bases.First;
                default:
                    throw new Exception($"{defender.GetMinimalInfo()} is not at a defensive position");
            }
        }

        private Bases GetOptimalBaseForThrow(HitDirections hitDirection, Batter defender)
        {
            var occupiedBaseCount = GetOccupiedBaseCount();
            var baseDefenderClosestTo = GetBaseDefenderClosestTo(defender);
            var mostAdvancedBaseBeingRunTo = GetMostAdvancedBaseBeingRunTo();

            // Handle bases loaded scenario
            switch(occupiedBaseCount)
            {
                case 4:
                    // Always throw to nearest base
                    return baseDefenderClosestTo;
                case 3:
                case 2:
                    // Throw to the most advanced base unless the fielder is closer to someone
                    if(baseDefenderClosestTo == Bases.First && IsBatterAtBase((int)Bases.Home))
                    {
                        return Bases.First;
                    }
                    else if(baseDefenderClosestTo == Bases.Second && IsBatterAtBase((int)Bases.First))
                    {
                        return Bases.Second;
                    }
                    return mostAdvancedBaseBeingRunTo;
                case 1:
                    // Always throw to most advanced base
                    return mostAdvancedBaseBeingRunTo;
                default:
                    throw new Exception($"{occupiedBaseCount} is an invalid occupied base count");
            }
        }

        private Batter GetFielderForDirection(HitDirections direction)
        {
            switch(direction)
            {
                case HitDirections.LeftField:
                    return game.teamDefending.batters[BatterPositions.LeftField];
                case HitDirections.CenterField:
                    return game.teamDefending.batters[BatterPositions.CenterField];
                case HitDirections.RightField:
                    return game.teamDefending.batters[BatterPositions.CenterField];
                default:
                    throw new Exception("This method should not be called for non-field directions.");
            }
        }
        private int GetCatchScore(Batter fielder)
        {
            var random = Randomizer.GetRandom(1, 100);
            return random + ((fielder.speed + fielder.vision) / 2);
        }

        private bool HandleCatch(int fielderScore, Batter fielder)
        {
            if(fielderScore > 190)
            {
                Output($"{fielder.GetMinimalInfo()} hauled it in -- what an incredible catch! {outs} out(s).");
                return true;
            }
            else if(fielderScore > 170)
            {
                Output($"{fielder.GetMinimalInfo()} caught the ball with a brilliant effort! {outs} out(s).");
                return true;
            }
            else if(fielderScore > 140)
            {
                Output($"{fielder.GetMinimalInfo()} with a solid catch! {outs} out(s).");
                return true;
            }
            else
            {
                Output($"{fielder.GetMinimalInfo()} retrieved it from the outfield.");
                return false;
            }
        }
        private void IncrementOuts()
        {
            outs += 1;
            strikes = 0;
            balls = 0;
            if(outs >= 3)
            {
                Output($@"That will do it for this half. 
                {game.homeTeam.name} {game.homeScore}, {game.awayTeam.name} {game.awayScore}.");
                isHalfInningComplete = true;
            }
        }

        private int GetOccupiedBaseCount()
        {
            var count = 0;
            for(int i = 0; i < battersOnBases.Length - 1; i++)
            {
                if(IsBatterAtBase(i))
                {
                    count += 1;
                }
            }
            return count;
        }
        private Bases GetMostAdvancedBaseBeingRunTo()
        {
            var index = 0;
            for(int i = battersOnBases.Length - 1; i >= 0; i--)
            {
                if(IsBatterAtBase(i))
                {
                    index = i + 1;
                    if(index == battersOnBases.Length)
                    {
                        index = 0;
                    }
                    return (Bases) index;
                }
            }
            throw new Exception("No bases are being run to");
        }

        private Batter GetBasemanForPosition(int basePositionIndex)
        {
            switch(basePositionIndex)
            {
                case 0:
                    return game.teamDefending.batters[BatterPositions.Catcher];
                case 1:
                    return game.teamDefending.batters[BatterPositions.FirstBase];
                case 2:
                    return game.teamDefending.batters[BatterPositions.SecondBase];
                case 3:
                    return game.teamDefending.batters[BatterPositions.ThirdBase];
                default:
                    throw new Exception($"Unknown baseman position {basePositionIndex}");
            }
        }

        private void OutputBaseStatus()
        {
            Output("Base Status: ===");
            for(var i = 0; i < Enum.GetValues(typeof(Bases)).Length; i++)
            {
                switch(i)
                {
                    case (int)Bases.Home:
                        var homeInfo = IsBatterAtBase((int)Bases.Home) ? GetBatterAtBase((int)Bases.Home).GetMinimalInfo() : "Empty";
                        Console.WriteLine($"Home: {homeInfo}");
                    break;
                    case (int)Bases.First:
                        var firstInfo = IsBatterAtBase((int)Bases.Home) ? GetBatterAtBase((int)Bases.Home).GetMinimalInfo() : "Empty";
                        Console.WriteLine($"1st: {firstInfo}");
                    break;
                    case (int)Bases.Second:
                        var secondInfo = IsBatterAtBase((int)Bases.Second) ? GetBatterAtBase((int)Bases.Second).GetMinimalInfo() : "Empty";
                        Console.WriteLine($"2nd: {secondInfo}");
                    break;
                    case (int)Bases.Third:
                        var thirdInfo = IsBatterAtBase((int)Bases.Second) ? GetBatterAtBase((int)Bases.Second).GetMinimalInfo() : "Empty";
                        Console.WriteLine($"3rd: {thirdInfo}");
                    break;
                }
            }
            Output(string.Empty);
        }

        private bool IsBatterAtBase(int baseIndex)
        {
            return battersOnBases[baseIndex] != -1;
        }

        private bool IsBatterAtBase(Bases baseToCheck)
        {
            return battersOnBases[(int)baseToCheck] != -1;
        }

        private Batter GetBatterAtBase(int baseIndex)
        {
            if(baseIndex > 3 || baseIndex < 0)
            {
                throw new Exception("Index not allowed");
            }
            return game.teamAtBat.batters[(BatterPositions)battersOnBases[baseIndex]];
        }

        private void SetBaseToEmpty(int baseIndex)
        {
            if(baseIndex > 3 || baseIndex < 0)
            {
                throw new Exception("Index not allowed");
            }
            battersOnBases[baseIndex] = -1;
        }

        private void SetBatterAtBase(int baseIndex, Batter batter)
        {
            if(baseIndex > 3 || baseIndex < 0)
            {
                throw new Exception("Index not allowed");
            }
            if(batter == null)
            {
                battersOnBases[baseIndex] = -1;
                return;
            }
            battersOnBases[baseIndex] = (int)batter.position;
        }
    }
}