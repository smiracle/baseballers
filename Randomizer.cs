using System;

namespace Baseballers
{
    public static class Randomizer
    {
        private static Random random = new Random();
        public static int GetRandom(int min, int max)
        {
            return random.Next(min, max);
        }
    }
}