using System;

namespace Baseballers
{
    public enum PitcherPositions 
    {
        Starting,
        Relief,
        Closer,
        Bench
    }
}