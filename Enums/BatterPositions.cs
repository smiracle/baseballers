using System;

namespace Baseballers
{
    public enum BatterPositions 
    {
        DesignatedHitter,
        Catcher,
        ShortStop,
        FirstBase,
        SecondBase,
        ThirdBase,
        LeftField,
        CenterField,
        RightField,
        Bench
    }
}