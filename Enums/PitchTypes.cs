using System;

namespace Baseballers
{
    public enum PitchTypes
    {
       Fastball = 0,
       Curveball = 1,
       Slider = 2,
       Meatball = 3
    }
}