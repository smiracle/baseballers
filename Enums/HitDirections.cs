using System;

namespace Baseballers
{
    public enum HitDirections 
    {
        FoulLeft,
        FoulRight,
        LeftField,
        CenterField,
        RightField,
    }
}