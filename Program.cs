﻿using System;
using System.Collections.Generic;

namespace Baseballers
{
    class Program
    {
        static void Main(string[] args)
        {
            var playerTeam = new Team();
            Console.WriteLine("Your Team: ");
            playerTeam.OutputRoster();
            
            var opponentTeams = new List<Team>();
            const int numberOfOpposingTeams = 10;
            for(int i = 0; i < numberOfOpposingTeams; i++)
            {
                opponentTeams.Add(new Team());
            }

            var opposingTeam = opponentTeams[Randomizer.GetRandom(0, opponentTeams.Count)];
            Console.WriteLine("Opposing Team: ");
            opposingTeam.OutputRoster();
            new BaseballGame(playerTeam, opposingTeam);
            Console.ReadLine();
        }
    }    
}
 