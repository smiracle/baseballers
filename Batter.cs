using System;

namespace Baseballers
{
    public class Batter : Player
    {
        public int power;
        public int vision;
        public int speed;
        public BatterPositions position;
        public Batter() : base()
        {            
            power = Randomizer.GetRandom(1, 100);
            vision = Randomizer.GetRandom(1, 100);
            speed = Randomizer.GetRandom(1, 100);
        }

        public string GetMinimalInfo()
        {
            return $"{firstName} {lastName} ({Names.batterShortPositions[position]})";
        }
        
        public void OutputAllStats()
        {
            Console.WriteLine($"{firstName} {lastName} ({position}) Pwr {power}, Vis {vision}, Spd {speed}");
        }
    }
}