using System;
using System.Linq;
using System.Collections.Generic;

namespace Baseballers
{
    public class Team
    {
        public string name;
        public int seasonWins;
        public int seasonLosses;
        public Dictionary<PitcherPositions, Pitcher> pitchers = new Dictionary<PitcherPositions, Pitcher>();
        public Dictionary<BatterPositions, Batter> batters = new Dictionary<BatterPositions, Batter>();
        public List<BatterPositions> battingLineup = new List<BatterPositions>();
        public Pitcher activePitcher;

        public int batterIndex;

        public Team()
        {
            name = Names.GetRandomTeamName();
            var unassignedBatters = GenerateBatters();
            battingLineup = AssignBattersToOptimalPositions(ref unassignedBatters);
            
            var unassignedPitchers = GeneratePitchers();
            AssignPitchersToOptimalPositions(ref unassignedPitchers);            
        }

        public void OutputRoster()
        {
            Console.WriteLine($"=={name}==");
            Console.WriteLine("Pitching lineup: ");
            foreach(var pitcher in pitchers.Values)
            {
                pitcher.OutputAllStats();
            }
            Console.WriteLine("Batting lineup: ");
            foreach(var batter in batters.Values)
            {
                batter.OutputAllStats();
            }
            Console.WriteLine();
        }

        private List<Pitcher> GeneratePitchers()
        {
            var unassignedPitchers = new List<Pitcher>();
            for(int i = 0; i < Enum.GetValues(typeof(PitcherPositions)).Length; i++)
            {
                unassignedPitchers.Add(new Pitcher());
            }
            return unassignedPitchers;
        }

        public Batter GetNextBatterInLineup()
        {
            batterIndex += 1;
            // Minus one extra due to benched player
            if(batterIndex >= batters.Values.Count - 1)
            {
                batterIndex = 0;
            }
            return batters[battingLineup[batterIndex]];
        }

        public Pitcher GetNextPitcherInLineup()
        {
            if(activePitcher.position == PitcherPositions.Starting)
            {
                return pitchers[PitcherPositions.Relief];
            }
            else if(activePitcher.position == PitcherPositions.Relief)
            {
                return pitchers[PitcherPositions.Closer];
            }
            else
            {
                return pitchers[PitcherPositions.Starting];
            }
        }

        private List<Batter> GenerateBatters()
        {
            var unassignedBatters = new List<Batter>();
            for(int i = 0; i < Enum.GetValues(typeof(BatterPositions)).Length; i++)
            {
                unassignedBatters.Add(new Batter());
            }
            return unassignedBatters;
        }

        private void AssignPitchersToOptimalPositions(ref List<Pitcher> unassignedPitchers)
        {
            for(int i = 0; i < Enum.GetValues(typeof(PitcherPositions)).Length; i++)
            {
                switch((PitcherPositions) i)
                {
                    case PitcherPositions.Starting:
                        var s = GetBestRemainingAccuracy(ref unassignedPitchers);
                        s.position = PitcherPositions.Starting;
                        pitchers.Add(PitcherPositions.Starting, s);
                    break;
                    case PitcherPositions.Relief:
                        var r = GetBestRemainingAccuracy(ref unassignedPitchers);
                        r.position = PitcherPositions.Relief;
                        pitchers.Add(PitcherPositions.Relief, r);
                    break;
                    case PitcherPositions.Closer:
                        var c = GetBestRemainingAccuracy(ref unassignedPitchers);
                        c.position = PitcherPositions.Closer;
                        pitchers.Add(PitcherPositions.Closer, c);
                    break;
                    case PitcherPositions.Bench:
                        var b = GetBestRemainingAccuracy(ref unassignedPitchers);
                        b.position = PitcherPositions.Bench;
                        pitchers.Add(PitcherPositions.Bench, b);
                    break;
                }
            }
        }

        private List<BatterPositions> AssignBattersToOptimalPositions(ref List<Batter> unassignedBatters)
        {
            var lineup = new List<Batter>();
            for(int i = 0 ; i < Enum.GetValues(typeof(BatterPositions)).Length; i++)
            {
                switch((BatterPositions)i)
                {
                    case BatterPositions.DesignatedHitter:
                        var dHitter = GetBestRemainingPower(ref unassignedBatters);
                        dHitter.position = BatterPositions.DesignatedHitter;
                        batters.Add(BatterPositions.DesignatedHitter, dHitter);
                        lineup.Add(dHitter);
                        break;
                    case BatterPositions.Catcher:                        
                        var catcher = GetBestRemainingSpeedAndVision(ref unassignedBatters);
                        catcher.position = BatterPositions.Catcher;
                        batters.Add(BatterPositions.Catcher, catcher);
                        lineup.Add(catcher);
                        break;
                    case BatterPositions.ShortStop:
                        var ss = GetBestRemainingSpeedAndVision(ref unassignedBatters);
                        ss.position = BatterPositions.ShortStop;
                        batters.Add(BatterPositions.ShortStop, ss);
                        lineup.Add(ss);
                        break;
                    case BatterPositions.FirstBase:
                        var fb = GetBestRemainingSpeedAndVision(ref unassignedBatters);
                        fb.position = BatterPositions.FirstBase;
                        batters.Add(BatterPositions.FirstBase, fb);
                        lineup.Add(fb);
                        break;
                    case BatterPositions.SecondBase:
                        var sb = GetBestRemainingSpeedAndVision(ref unassignedBatters);
                        sb.position = BatterPositions.SecondBase;
                        batters.Add(BatterPositions.SecondBase, sb);
                        lineup.Add(sb);
                        break;
                    case BatterPositions.ThirdBase:
                        var tb = GetBestRemainingSpeedAndVision(ref unassignedBatters);
                        tb.position = BatterPositions.ThirdBase;
                        batters.Add(BatterPositions.ThirdBase, tb);
                        lineup.Add(tb);
                        break;
                    case BatterPositions.LeftField:
                        var lf = GetBestRemainingSpeedAndVision(ref unassignedBatters);
                        lf.position = BatterPositions.LeftField;
                        batters.Add(BatterPositions.LeftField, lf);
                        lineup.Add(lf);
                        break;
                    case BatterPositions.CenterField:
                        var cf = GetBestRemainingSpeedAndVision(ref unassignedBatters);
                        cf.position = BatterPositions.CenterField;
                        batters.Add(BatterPositions.CenterField, cf);
                        lineup.Add(cf);
                        break;
                    case BatterPositions.RightField:
                        var rf = GetBestRemainingSpeedAndVision(ref unassignedBatters);
                        rf.position = BatterPositions.RightField;
                        batters.Add(BatterPositions.RightField, rf);
                        lineup.Add(rf);
                        break;
                    case BatterPositions.Bench:
                        var b = GetBestRemainingSpeedAndVision(ref unassignedBatters);
                        b.position = BatterPositions.Bench;
                        batters.Add(BatterPositions.Bench, b);
                        // Don't add benched players to the lineup
                        break;
                }
            }
            return lineup.OrderByDescending(x => x.power).Select(x => x.position).ToList();
        }

        private Pitcher GetBestRemainingAccuracy(ref List<Pitcher> unassignedPitchers)
        {
            unassignedPitchers = unassignedPitchers.OrderByDescending(x => x.accuracy).ToList();
            var best = unassignedPitchers.First();
            unassignedPitchers.Remove(best);
            return best;
        }

        private Batter GetBestRemainingPower(ref List<Batter> unassignedBatters)
        {
            unassignedBatters = unassignedBatters.OrderByDescending(x => x.power).ToList();
            var best = unassignedBatters.First();
            unassignedBatters.Remove(best);
            return best;
        }

        private Batter GetBestRemainingSpeedAndVision(ref List<Batter> unassignedBatters)
        {
            unassignedBatters = unassignedBatters.OrderByDescending(x => x.vision + x.speed).ToList();
            var best = unassignedBatters.First();
            unassignedBatters.Remove(best);
            return best;
        }

        private Batter GetBestRemainingSpeed(ref List<Batter> unassignedBatters)
        {
            unassignedBatters = unassignedBatters.OrderByDescending(x => x.speed).ToList();
            var best = unassignedBatters.First();
            unassignedBatters.Remove(best);
            return best;
        }
    }
}