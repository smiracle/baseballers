using System;

namespace Baseballers
{
    public class Player
    {                
        public string firstName;
        public string lastName;
        public int energy;
        public Player()
        {
            firstName = Names.GetRandomFirstName();
            lastName = Names.GetRandomLastName();
            energy = 100;
        }
    }
}